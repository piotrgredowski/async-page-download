autopep8==1.4.3
coverage==4.5.2
fakeredis==0.16.0
ipython==7.1.1
nose==1.3.7
pylint==2.2.1
